/*
 license: The MIT License, Copyright (c) 2024 ClearCode Inc.
 original:
   https://gitlab.com/clear-code/webextensions-lib-cross-session-state
*/

'use strict';

class CrossSessionState {
  constructor(definitions) {
    this.resumed = false;

    this._definitions = definitions;
    this._values = {};

    for (const [key, definition] of Object.entries(definitions)) {
      this._values[key] = definition.default;
      Object.defineProperty(this, key, {
        get: () => this._getValue(key),
        set: (value) => this._setValue(key, value),
        enumerable: true,
      });
      if (typeof definition.set !== 'function')
        definition.set = (value) => value;
      if (typeof definition.get !== 'function')
        definition.get = (value) => value;
    }

    this._restoreAll();
  }

  save(...keys) {
    if (keys.length == 0)
      keys = Object.keys(this._definitions);
    const dataToSave = Object.fromEntries(
      keys.map(key => [key, this._definitions[key].set(this._values[key])])
    );
    chrome.storage.session.set(dataToSave);
  }

  _getValue(key) {
    return this._values[key];
  }

  _setValue(key, value) {
    this._values[key] = value;
    this.save(key);
    return value;
  }

  async _restoreAll() {
    if (this.restored)
      return this.restored;

    return this.restored = new Promise(async (resolve, _reject) => {
      try {
        const restoredValues = await chrome.storage.session.get(
          Object.fromEntries(Object.keys(this._definitions).map(key => [key, null]))
        );
        for (const [key, restoredValue] of Object.entries(restoredValues)) {
          if (restoredValue === null)
            continue;

          if (!this.resumed &&
              restoredValue !== null)
            this.resumed = true;

          this._values[key] = this._definitions[key].get(restoredValue);
        }
      }
      catch(error) {
        console.log('Failed to restore previous state: ', error.name, error.message);
      }
      resolve();
    });
  }
}
