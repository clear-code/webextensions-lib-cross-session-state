# webextensions-lib-cross-session-state

A small library to save/restore state across sessions for short-live Service Workers on Manifest V3 Chromium extensions.

## Required permissions

* `storage`

## Usage

```javascript
const state = new CrossSessionState({
  count: { // key
    default: 0, // default value
  },
  // non JSON-stringifiable objects need to be converted.
  // for example, Set:
  ids: {
    default: new Set(),
    set: value => [...value], // converter function to save value to the storage
    get: value => new Set(value), // converter function to restore value from the storage
  },
  // nested Map:
  idToNames: {
    default: new Map([
      [1, new Map([
        ['a', 10],
        ['b', 20],
      ])],
      [2, new Map([
        ['c', 30],
        ['d', 40],
      ])],
    ]),
    set: value => Array.from(
      value.entries(),
      ([id, names]) => [id, [...names.entries()]]
    ),
    get: value => new Map(
      value.map((id, nameEntries) => [id, new Map(nameEntries)])
    ),
  },
});

async function doSomething(id, name, priority) {
  await state.restored; // wait until previous state is restored

  state.count = state.count + 1; // assignment will save the updated value

  state.ids.add(id);

  const names = state.idToName.get(id) || new Set();
  names.set(name, priority);
  state.idToName.set(id, names);

  // nested values won't be saved automatically so you need to kick it
  state.save('ids', 'idToName');
  // state.save(); // calling without keys will save all entries.
}
```

## License

The MIT License.
